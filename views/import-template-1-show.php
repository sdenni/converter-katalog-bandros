<?php

$array_data = array();

include_once "../php/method/import-template-1.php";


?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin 2 - Blank</title>

  <!-- Custom fonts for this template-->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php include_once "../layouts/sidebar.html" ?>
    
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php include_once "../layouts/topbar.html" ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Import Template 1</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Lihat Contoh</a>
          </div>
          
          <div class='row'>
            <div class='col'>
            <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nama Produk<br /><small style="color: green;"><?php echo $fields[0] ?></small></th>
                      <th>Kode Produk<br /><small style="color: green;"><?php echo $fields[1] ?></small></th>
                      <th>Deskripsi<br /><small style="color: green;"><?php echo $fields[2] ?></small></th>
                      <th>Ukuran<br /><small style="color: green;"><?php echo $fields[3] ?></small></th>
                      <th>Gambar<br /><small style="color: green;"><?php echo $fields[4] ?></small></th>
                      <th>Harga Jual<br /><small style="color: green;"><?php echo $fields[5] ?></small></th>
                      <th>Harga Member<br /><small style="color: green;"><?php echo $fields[6] ?></small></th>
                      <th>Berat (Gram)<br /><small style="color: green;"><?php echo $fields[7] ?></small></th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Nama Produk<br /><small style="color: green;"><?php echo $fields[0] ?></small></th>
                      <th>Kode Produk<br /><small style="color: green;"><?php echo $fields[1] ?></small></th>
                      <th>Deskripsi<br /><small style="color: green;"><?php echo $fields[2] ?></small></th>
                      <th>Ukuran<br /><small style="color: green;"><?php echo $fields[3] ?></small></th>
                      <th>Gambar<br /><small style="color: green;"><?php echo $fields[4] ?></small></th>
                      <th>Harga Jual<br /><small style="color: green;"><?php echo $fields[5] ?></small></th>
                      <th>Harga Member<br /><small style="color: green;"><?php echo $fields[6] ?></small></th>
                      <th>Berat (Gram)<br /><small style="color: green;"><?php echo $fields[7] ?></small></th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php
                    
                        foreach ($array_data as $key => $value) {
                            echo "
                                  <tr>
                                    <td>$value[0]</td>
                                    <td>$value[1]</td>
                                    <td wrap>$value[2]</td>
                                    <td>$value[3]</td>
                                    <td>$value[4]</td>
                                    <td>$value[5]</td>
                                    <td>$value[6]</td>
                                    <td>$value[7]</td>
                                  </tr>
                            ";
                        }
                    
                    ?>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php include_once "../layouts/footer.html"?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>

</body>

</html>

<?php



class Database 
{
    function __construct($serverName="", $dbUser="", $dbPassword="", $dbName="") {
        return $this->Connection($serverName, $dbUser, $dbPassword, $dbName);
    }
    
    function Connection($serverName="", $dbUser="", $dbPassword="", $dbName=""){
    
        if(trim($serverName) == "" ){
            $serverName = "localhost";
        }
        if(trim($dbUser) == "" ){
            $dbUser = "root";
        }
        if(trim($dbPassword) == "" ){
            $dbPassword = "mysql";
        }
        if(trim($dbName) == "" ){
            $dbName = "data_katalog";
        }
        
    
        try {
            $url = 'mysql:host='.$serverName.';dbname='.$dbName.'';
            
            $this->connection = new PDO($url, $dbUser, $dbPassword);
            // $this->connection = new PDO("mysql:host={$serverName};dbname={$dbName}", $username, $password);
            // return $connection;
            
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    
    function ShowDatas($query){
    }
    
    function InsertDatas($object){
        /**
         * format Object
         * object = { table_name : "table_name", field: {field1, field2, field3}, values: [{value1, value2, value3},{value1, value2, value3}] } 
         */
        
        $query = $this->GenerateInsertDatas($object);
        return $this->connection->query($query);
        
    }
    
    function GenerateInsertDatas($object){
        /**
         * format Object
         * object = { table_name : "table_name", field: {field1, field2, field3}, values: [{value1, value2, value3},{value1, value2, value3}] } 
         */
        
        $arr_objects = json_decode($object);
        $fields_generated = "";
        
        // $fields_count = count($arr_objects['field']);
        // for ($i=0; $i < $fields_count; $i++) { 
        //     if($i == 0){
        //         $fields_generated = ""
        //     }
        // } 
        
        //FIELD GENERATOR
        $fields_generated = $this->GenerateField($arr_objects->field);
        
        
        //VALUES GENERATOR
        $values_generated = $this->GenerateValues($arr_objects->values);
        
        
        $query = "INSERT INTO ".$arr_objects->table_name." ($fields_generated) VALUES $values_generated";
        
        return $query;
    }
    
    private function GenerateField($field){
        $result = "";
        
        $first = true;
        foreach ($field as $key => $val) {
            if(!$first){
                $result .= ", $val ";
            } else if($first) {
                $result .= "$val ";
                $first = false;
            }
        }
        
        return $result;
    }
    
    private function GenerateValues($values){
        $result = "";
        
        $first = true;
        foreach ($values as $key => $val) {
            
            if(!$first){
                $result .= ", ".$this->GenerateValuesIndividu($val)." ";
            } else if($first) {
                $result .= $this->GenerateValuesIndividu($val);
                $first = false;
            }
        }
        return $result;
    }
    
    private function GenerateValuesIndividu($value){
        $result = "(";
        
        $first = true;
        foreach ($value as $key => $val) {
            if(!$first){
                $result .= ", '$val' ";
            } else if($first) {
                $result .= "'$val' ";
                $first = false;
            }
        }
        $result .= ")";
        
        return $result;
    }
    
}



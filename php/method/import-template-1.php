<?php

require '../vendor/autoload.php';

use \PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

require_once "../php/class/Database.php";

$target_dir = "uploads/";
// $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$target_file = $_FILES["fileToUpload"]["tmp_name"];
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image

$aksi = $_POST['aksi'];

// var_dump($_FILES["fileToUpload"]);
if(isset($_POST)) {

    
    
    
    
    if($aksi == "show-before-import"){
    
      $reader = new Xlsx();
      $reader->setReadDataOnly(true);
      $spreadsheet = $reader->load($target_file);
    
      $data = $spreadsheet->getActiveSheet()->toArray();
    $values = [];
    $fields = [];
    
      
      foreach($data as $row)
      {
            $row_count++;
            if($row_count>1){
                  $values[] = array($row[1], $row[2], $row[3], $row[4], $row[5], $row[6], $row[7], $row[8]);
            } else {
                  $fields = array($row[1], $row[2], $row[3], $row[4], $row[5], $row[6], $row[7], $row[8]);
            }            
            
      }
      
      $array_data = $values;
      
    }
    
    if($aksi == "import"){
      $db = new Database();
      
      $row_count=0;
      foreach($data as $row)
      {
            $row_count++;
            if($row_count>1){
                  $values[] = array($row[1], $row[2], $row[3], $row[4], $row[5], $row[6], $row[7], $row[8]);
            }            
      }
      
      $arr_objects = array(
            'table_name'=>'katalog',
            'field'=>array(
                  'nama', 'kode', 'keterangan', 'ukuran', 'gambar', 'harga_jual', 'harga_member', 'berat'
            ),
            'values'=> $values
      );
      
      if($db->InsertDatas(json_encode($arr_objects))) {
            header('Location: /');
      } else {
            var_dump("gagal");
      }
      
    }
    
  
}


-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.37 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for data_katalog
CREATE DATABASE IF NOT EXISTS `data_katalog` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `data_katalog`;

-- Dumping structure for table data_katalog.katalog
DROP TABLE IF EXISTS `katalog`;
CREATE TABLE IF NOT EXISTS `katalog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `ukuran` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `harga_jual` mediumint(8) unsigned NOT NULL,
  `harga_member` mediumint(8) unsigned NOT NULL,
  `berat` smallint(5) unsigned NOT NULL,
  `custom_kode` smallint(5) unsigned DEFAULT NULL,
  `custom_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_keterangan` smallint(5) unsigned DEFAULT NULL,
  `custom_harga_jual` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table data_katalog.katalog: ~4 rows (approximately)
/*!40000 ALTER TABLE `katalog` DISABLE KEYS */;
/*!40000 ALTER TABLE `katalog` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
